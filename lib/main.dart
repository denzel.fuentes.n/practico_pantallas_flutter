import 'package:flutter/material.dart';

void main() {
  runApp(const MyWidget());
}

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: true,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
              tabs: [
                Tab(
                  text: "TRIPS",
                ),
                Tab(
                  text: "HOTELS",
                )
              ],
            ),
          ),
          body: TabBarView(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Card(
                        color: Colors.white,
                        child: Container(
                          padding: const EdgeInsets.all(25),
                          child: Row(
                            children: [
                              Container(
                                width: 65,
                                height: 65,
                                margin:
                                    const EdgeInsets.only(left: 0, right: 25.0),
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromARGB(246, 255, 218, 230),
                                ),
                                child: const Center(
                                    child: Icon(
                                  Icons.airplanemode_active,
                                  color: Colors.pink,
                                  size: 28,
                                )),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        const Text(
                                          "Hawaii",
                                          style: TextStyle(
                                              fontSize: 19.0,
                                              fontFamily: 'Roboto',
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              left: 20,
                                              right:
                                                  10), // Margen en todos los lados del icono
                                          child: const Icon(Icons.arrow_forward,
                                              color: Color.fromARGB(
                                                  255, 190, 188, 188)),
                                        ),
                                        const Text(
                                          "Sevilla",
                                          style: TextStyle(
                                              fontSize: 19.0,
                                              fontFamily: 'Roboto',
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        const Text(
                                          "28 Oct, 2018",
                                          style: TextStyle(
                                              fontFamily: "Roboto",
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(left: 20,right: 20),
                                          child: const  Text(
                                            "---",
                                            style: TextStyle(
                                              fontFamily: "Roboto",
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                          )
                                        ),
                                        const Text(
                                          "11:00AM",
                                          style: TextStyle(
                                              fontFamily: "Roboto",
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                        ),                                        
                                      ],
                                    ),
                                    Text(
                                      "Emirates Airways",
                                      style: TextStyle(
                                          fontFamily: "Roboto",
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                              Icon(Icons.more_vert),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              Text("data"),
            ],
          ),
        ),
      ),
    );
  }
}
